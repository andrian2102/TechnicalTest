const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Enter an array of numbers separated by space: ', (input) => {
  const arr = input.trim().split(' ').map(num => parseInt(num));
  const unique = uniqueNum(arr);
  console.log(`The unique number is: ${unique}`);
  rl.close();
});

function uniqueNum(arr) {
  let seen = new Set();
  let uniques = new Set();
  for (let i = 0; i < arr.length; i++) {
    if (seen.has(arr[i])) {
      uniques.delete(arr[i]);
    } else {
      seen.add(arr[i]);
      uniques.add(arr[i]);
    }
  }

  return Array.from(uniques);
}
