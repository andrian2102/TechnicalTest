import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Produk } from './entities/produk.entity';
import { CreateProdukDto } from './dto/create-produk.dto';
import { UpdateProdukDto } from './dto/update-produk.dto';

@Injectable()
export class ProdukService {
  constructor(
    @InjectRepository(Produk)
    private produkRepository: Repository<Produk>,
  ) {}

  async findAll(page: number = 1, limit: number = 10) {
    const [products, count] = await this.produkRepository.findAndCount({
      take: limit,
      skip: (page - 1) * limit,
    });

    const totalPages = Math.ceil (count/limit);
    return {
      data: products,
      page,
      totalPages,
      totalItems: count,
    };
  }

  async findOne(id: number): Promise<Produk> {
    return this.produkRepository.findOne({where: {id}});
  }

  async create(produk: Produk): Promise<Produk> {
    return this.produkRepository.save(produk);
  }

  async update(id: number, produk: Produk): Promise<void> {
    await this.produkRepository.update(id, produk);
  }

  async delete(id: number): Promise<void> {
    await this.produkRepository.delete(id);
  }
}
