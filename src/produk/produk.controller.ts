import { Controller, Get, Query, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { Produk } from './entities/produk.entity';
import { ProdukService } from './produk.service';
import { CreateProdukDto } from './dto/create-produk.dto';
import { UpdateProdukDto } from './dto/update-produk.dto';

@Controller('produk')
export class ProdukController {
  constructor(private produkService: ProdukService) {}

  @Get()
  async findAll(@Query('page') page: number, @Query('limit') limit: number) {
    return this.produkService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Produk> {
    return this.produkService.findOne(id);
  }

  @Post()
  async create(@Body() produk: Produk): Promise<Produk> {
    return this.produkService.create(produk);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() produk: Produk): Promise<void> {
    await this.produkService.update(id, produk);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<void> {
    await this.produkService.delete(id);
  }
}
