import { Entity, CreateDateColumn, UpdateDateColumn, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Produk {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 200, nullable: false })
  nama_barang: string;

  @Column({ type: 'varchar', length: 50, nullable: false })
  kode_barang: string;

  @Column({ nullable: false })
  jumlah_barang: number;

  @CreateDateColumn({ nullable: false })
  create_date: Date;

  @UpdateDateColumn({ nullable: false })
  update_date: Date;
}
