import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Produk } from './entities/produk.entity';
import { ProdukService } from './produk.service';
import { ProdukController } from './produk.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '',
      database: 'TechnicalTest',
      entities: [Produk],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Produk]),
  ],
  controllers: [ProdukController],
  providers: [ProdukService]
})
export class ProdukModule {}
